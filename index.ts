// Download wordlist.txt from http://www-personal.umich.edu/~jlawler/wordlist
// Thanks to http://www-personal.umich.edu/~jlawler/wordlist.html

import _ from "lodash";
import { readFileSync } from "fs";

//const puzzleInput = "FEYI MY IRAYD ZRA CWMTISTD IRD YMPD CWAXDYYSAU WDTAKUSQD DMTR AIRDW SUYISUTISLDVH, YA OA IRAYD ZRA CWMTISTD IRD YMPD LSTD."
//const puzzleInput = "J RNQFWXAC NYRA MAJX QA JX RSAKK, MWX VX IJK YN QJXRS LNC QA JY TVRT MNEVYP. - AQN FSVDVFK";
//const puzzleInput = "WBM UEQ NL UV UFPHQMVW NF OEGZHGGENV GBNHKO VNW JM XEZWNFA, JHW CFNPFMGG.";
//const puzzleInput = "J RNQFWXAC NYRA MAJX QA JX RSAKK, MWX VX IJK YN QJXRS LNC QA JX TVRT MNEVYP.";
const puzzleInput = "EQA SGBEU DSKG AQ UQWGYE GWGEAO, B UQWGYE FNOGKZ.";

const readDictionary = (filename: string): string[] => {
    const contents = readFileSync(filename, { encoding: "utf-8" });
    return contents.split(/\r?\n/);
};

type WordShape = string;
const getWordShape = (word: string): WordShape => {
    const getFirstIndexOfLetter = _.memoize((_letter: string, index: number) => index, (letter: string, _index: number) => letter);
    return _.map(word, getFirstIndexOfLetter).join("-");
};

const groupWordsByShape = (words: string[]): Record<WordShape, string[]> => {
    const stripNonAlphaCharacters = (x: string) => x.replace(/[^a-z]/ig, "");
    const validWords = words.map(stripNonAlphaCharacters).filter(x => x);
    return _.groupBy(validWords, getWordShape);
}

const renderPuzzleText = (puzzleInput: string, substitutionMap: Record<string, string>) => {
    const isLetterRegex = /[a-z]/i;
    return _.map(puzzleInput, x => isLetterRegex.test(x)
        ? substitutionMap[x.toLowerCase()] || "?"
        : x).join("");
};

const solveCryptogram = (puzzleInput: string, dictionary: string[]): string => {
    const sanitizedPuzzleInput = puzzleInput.toLowerCase();
    const puzzleWords = _.uniq(_.words(sanitizedPuzzleInput).map(x => x.replace(/[^a-z].+$/, "")));
    const wordsByShapeLookup = groupWordsByShape(dictionary.map(x => x.toLowerCase()).filter(x => x));
    const wordDictionary = _.mapKeys(dictionary.map(x => x.toLowerCase()), x => x);
    const isDictionaryWord = (word: string) => wordDictionary[word] !== undefined;

    type CryptogramSolutionCandidate = { substitutionMap: Record<string, string>, unsolvableWords: number };
    let bestSolveUnsolvableWordCount = Number.MAX_VALUE;
    const solveCryptogramStep = (substitutionMap: Record<string, string>): CryptogramSolutionCandidate => {
        const getPuzzleWordSolutionCandidates = _.memoize((puzzleWord: string) => {
            const previouslyUsedLetterRegex = `[^${_.values(substitutionMap).join()}]`;
            const wordMatchRegexPattern = _.map(puzzleWord, x => substitutionMap[x] || previouslyUsedLetterRegex).join("");
            const wordMatchRegex = new RegExp(wordMatchRegexPattern);
            return (wordsByShapeLookup[getWordShape(puzzleWord)] || []).filter(x => wordMatchRegex.test(x));
        });

        // Given a map like {'a': 'x', 'b': 'y', 'c': 'z'}, we get the RegEx /^[abc]+$/, a regex representing
        // a word where every character from start (^) to end ($) comes from the set of solved characters.
        const isWordSolved = new RegExp(`^[${_.keys(substitutionMap).join("")}]+$`);
        const unsolvedWords = puzzleWords.filter(x => !isWordSolved.test(x));
        const unsolvableWords = unsolvedWords.filter(x => getPuzzleWordSolutionCandidates(x).length === 0);
        const solvableWords = unsolvedWords.filter(x => getPuzzleWordSolutionCandidates(x).length !== 0);
        const invalidWords = puzzleWords.filter(x => isWordSolved.test(x) && !isDictionaryWord(renderPuzzleText(x, substitutionMap)));
            
        const errorCount = invalidWords.length + unsolvableWords.length;
        if (errorCount >= bestSolveUnsolvableWordCount)
            return { substitutionMap: substitutionMap, unsolvableWords: errorCount };

        const nextPuzzleWord = _(solvableWords)
            .orderBy(x => _.uniq(x).length, "desc")
            .first();

        if (nextPuzzleWord === undefined) {
            bestSolveUnsolvableWordCount = Math.min(bestSolveUnsolvableWordCount, errorCount);
            return { substitutionMap: substitutionMap, unsolvableWords: errorCount };
        }

        const bestSolutionForPuzzleWord = _(getPuzzleWordSolutionCandidates(nextPuzzleWord))
            .map(x => solveCryptogramStep(_.assign(_.zipObject(nextPuzzleWord, x), substitutionMap)))
            .sortBy(x => x.unsolvableWords)
            .first()!;

        return bestSolutionForPuzzleWord;
    };

    const solvedCryptogram = solveCryptogramStep({});

    return renderPuzzleText(puzzleInput, solvedCryptogram.substitutionMap);
};

// Main?
const wordList = readDictionary("wordlist.txt");
const puzzleSolution = solveCryptogram(puzzleInput, wordList);
console.log(puzzleSolution);
